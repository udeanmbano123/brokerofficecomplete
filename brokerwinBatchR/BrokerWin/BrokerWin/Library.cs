﻿using BrokerWin.DDO;
using BrokerWin.DealClass;
using BrokerWin.DealNotes;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BrokerWin
{
    public static class Library
    {
        public static BrokerOfficeEntities db = new BrokerOfficeEntities();
        public static void WriteErrorLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }

        }
        

        

        public static bool deal(string s)
        {
           bool two = false;
            var p = 0;
            try
            {
              p = db.BatchDeals.ToList().Where(a => a.Deal == s).Count();

            }
            catch (Exception)
            {

                p = 0;
            }

            if (p > 0)
            {
                two = true;
            }
            else
            {
                two = false;
            }


            return two;
        }
        public static DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        

        public static void BatchDeals()
        {
            var sap = (from v in db.DealerDGs
                    select v).ToList().Where(a=>a.ID<=4195);
            WriteErrorLog("Deals"+ sap.ToList().Count);

            foreach (var zesa in sap)
            {
                try {
                    if (deal(zesa.Deal) == false)
                    {

                        
                        ////repost to transaction charges and trans
                        int? qty = Convert.ToInt32(zesa.Quantity);
                        int? qty2 = Convert.ToInt32(zesa.Quantity);
                        decimal? buys = Convert.ToDecimal(zesa.Price) * qty;
                        decimal? sells = Convert.ToDecimal(zesa.Price) * qty2;
                        string buyc = zesa.AccountName1;
                        string sellc = zesa.AccountName2;
                        string buyer = zesa.Account1;
                        string seller = zesa.Account2;
                        string sec = zesa.Security;
                        string sec2 = zesa.Security;
                        decimal? s = 0;

                        try
                        {
                            s = (decimal.Parse(sells.ToString(), CultureInfo.InvariantCulture));

                        }
                        catch (Exception)
                        {

                            s = 0;
                        }
                        decimal? b = s;
                        var p = db.TradingCharges.ToList();
                        foreach (var z in p)
                        {
                            TransactionCharge myyc = new TransactionCharge();

                            myyc.Transcode = zesa.Deal;
                            myyc.ChargeCode = z.chargecode;
                            if (z.chargecode.ToLower().Replace(" ", "") ==
                                ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {

                                myyc.BuyCharges = 0;
                            }
                            else
                            {
                                if (z.chargecode.ToLower().Replace(" ", "") ==
                                    ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                {
                                    myyc.BuyCharges = 0;
                                }
                                else
                                {
                                    myyc.BuyCharges = b * Convert.ToDecimal((z.chargevalue / 100));

                                }



                            }
                            if (z.chargecode.ToLower().Replace(" ", "") == ("Stamp duty").ToLower().Replace(" ", ""))
                            {
                                myyc.SellCharges = 0;

                            }
                            else
                            {
                                if (isTaxable(seller) == false && z.chargecode.ToLower().Replace(" ", "") ==
                                    ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                {
                                    myyc.SellCharges = 0;
                                }
                                else
                                {

                                    myyc.SellCharges = b * Convert.ToDecimal((z.chargevalue / 100));
                                }
                            }
                            string dgg = qty + " " + sec + " @ " + b;
                            myyc.Date = Convert.ToDateTime(zesa.PostDate);
                            postTransCharge(myyc.BuyCharges, myyc.SellCharges, myyc.ChargeCode, myyc.Transcode,
                                myyc.Date.ToString(), dgg);


                            db.TransactionCharges.Add(myyc);
                            db.SaveChanges();
                        }
                        //trans
                        //Post to accounts
                        string desc = qty + " " + sec + " @ " + b;
                        decimal? total = 0;
                        decimal? total2 = 0;
                        //insert charge to Trans
                        var zzz = db.TransactionCharges.ToList().Where(a => a.Transcode == zesa.Deal);
                        foreach (var f in zzz)
                        {

                            total += f.BuyCharges;


                            total2 += f.SellCharges;




                        }
                        string buy = "";
                        string buyaacount = "";
                        string sell = "";
                        string sellacount = "";
                        string sellA = "";
                        string sellacountA = "";
                        string buyA = "";
                        string buyaacountA = "";
                        var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
                        foreach (var ppc in pk)
                        {
                            buy = ppc.AccountName;
                            buyaacount = ppc.AccountNumber;
                        }
                        var tk = db.WindowsServices.ToList().Where(a => a.Action == "SELL");
                        foreach (var u in tk)
                        {
                            sell = u.AccountName;
                            sellacount = u.AccountNumber;
                        }
                        var pkA = db.WindowsServices.ToList().Where(a => a.Action == "BUYA");
                        foreach (var ppc in pkA)
                        {
                            buyA = ppc.AccountName;
                            buyaacountA = ppc.AccountNumber;
                        }
                        var tkA = db.WindowsServices.ToList().Where(a => a.Action == "SELLA");
                        foreach (var u in tkA)
                        {
                            sellA = u.AccountName;
                            sellacountA = u.AccountNumber;
                        }

                        decimal deals = 0;
                        decimal? bb = 0;
                        decimal? ss = 0;

                        //deals = Convert.ToDecimal((Math.Round(Convert.ToDouble(ppp.Price), 4) * Math.Round(Convert.ToDouble(ppp.Quantity), 4)));
                        deals = (decimal.Parse(buys.ToString(), CultureInfo.InvariantCulture));
                        bb = b + total;
                        ss = b - total2;
                        buys = (decimal.Parse(zesa.Price.ToString(), CultureInfo.InvariantCulture));
                        if (buyer != "ZSE")
                        {
                            Tran my2 = new Tran();
                            //debit account number
                            my2.Account = buyer;
                            if (my2.Account == null || my2.Account == "")
                            {
                                my2.Account = "ZSE";
                            }
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(zesa.Deal);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = zesa.Deal;
                            my2.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my2.Post = Convert.ToDateTime(zesa.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);
                            //Update table  
                            db.SaveChanges();

                            //update Acc Receivable
                            my2.Account = buyaacount;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(zesa.Deal);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = zesa.Deal;
                            my2.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my2.Post = Convert.ToDateTime(zesa.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);


                            db.SaveChanges();


                            my2.Account = buyaacountA;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = BR(zesa.Deal);
                            my2.Debit = 0;
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = zesa.Deal;
                            my2.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my2.Post = Convert.ToDateTime(zesa.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);


                            db.SaveChanges();

                        }
                        if (seller != "ZSE")
                        {
                            Tran my4 = new Tran();

                            //debit account number

                            my4.Account = seller;

                            if (my4.Account == "")
                            {
                                my4.Account = "ZSE";
                            }
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(zesa.Deal);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = zesa.Deal;
                            my4.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my4.Post = Convert.ToDateTime(zesa.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();
                            ////accounts payable
                            //debit account number
                            my4.Account = sellacount;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(zesa.Deal);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = zesa.Deal;
                            my4.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my4.Post = Convert.ToDateTime(zesa.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();

                            my4.Account = sellacountA;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = 0;
                            my4.Debit = BR(zesa.Deal);
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = zesa.Deal;
                            my4.TrxnDate = Convert.ToDateTime(zesa.PostDate);
                            my4.Post = Convert.ToDateTime(zesa.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();
                        }
                       

                        //add to batch deals
                        BatchDeal m5 = new BatchDeal();
                        m5.Deal = zesa.Deal;
                        m5.myDD = DateTime.Now;
                        db.BatchDeals.Add(m5);
                        db.SaveChanges();

                    }
                }
                catch (Exception ex)
                {
                    WriteErrorLog(ex.Message);
                    continue;
                }
            }
        }

        public static Decimal Buy(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')+sum(BuyCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public static Decimal Sell(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')-sum(SellCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }

            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public static Decimal BR(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT top 1 (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "') as 'Mo'  FROM TransactionCharges where transcode='" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }

        public static void PostCharges()
        {
            try
            {
                BatchDeals();
            }
            catch (Exception f)
            {

                WriteErrorLog(f.Message);
            }

        }

        public static string Setter(string s)
        {
            var dz = db.TradingCharges.ToList().Where(a => a.ChargeName.ToLower().Replace(" ","") == s.ToLower().Replace(" ", ""));
            string z = "";
            foreach (var p in dz)
            {
                z = p.tradeside;
            }

            return z;
        }

        public static List<TransactionCharge> GetEarners()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from [BrokerOffice].[dbo].[TransactionCharges]";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<TransactionCharge>();
            while (reader.Read())
            {
                var accountDetails = new TransactionCharge
                {

                    Id = Convert.ToInt32(reader.GetValue(0).ToString()),
                    ChargeCode = reader.GetValue(1).ToString(),

                    BuyCharges = Convert.ToDecimal(reader.GetValue(2).ToString()),
                    SellCharges = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    Date = Convert.ToDateTime(reader.GetValue(4).ToString()),
                    Transcode = reader.GetValue(5).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public static void postTransCharge(decimal? p, decimal? k, string c, string transcode, string st,string vv)
        {
            string buy = "";
            string buyaacount = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
            foreach (var t in pk)
            {
                buy = t.AccountName;
                buyaacount = t.AccountNumber;
            }

            var dz = db.TradingCharges.ToList().Where(a => a.ChargeName.ToLower().Replace(" ","") ==c.ToLower().Replace(" ", ""));
           
            foreach (var q in dz)
            {
                
                    Tran my3 = new Tran();

                    my3.Account = q.chargeaccountcode.ToString();
                    my3.Category = "Order Posting";
                    my3.Credit = p+k;
                    my3.Debit = 0;
                    my3.Narration = vv;
                    my3.Reference_Number = transcode;
                    my3.TrxnDate = Convert.ToDateTime(st);
                    my3.Post = Convert.ToDateTime(st);
                    my3.Type = q.chartaccount;
                    my3.PostedBy = "Background Service";
                    db.Trans.Add(my3);
                    db.SaveChanges();
                   


            }
        }

        public static void AddDeals()
        {
            StreamWriter sw = null;
            try
            {
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("settleds", Method.GET);

                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                List<Sealer> dataList = JsonConvert.DeserializeObject<List<Sealer>>(validate);


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creation on s.Account1 equals v.CDSC_Number
                            select s;
                //select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creation on s.Account2 equals v.CDSC_Number
                             select s;
                int six = 0;

                var allresults = dbsel.Concat(dbsel2).ToList().Distinct().OrderByDescending(a => a.Deal);

                
             
                foreach (var ppp in allresults)
                {
                    try
                    {
                        six = db.DealerDGs.ToList().Where(a => a.Deal == ppp.Deal).Count();

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }

                    if (six == 0)
                    {
                        DealerDG my = new DealerDG();
                        my.Account1 = ppp.Account1;
                        my.Account2 = ppp.Account2;
                        my.AccountName1 = ppp.AccountName1;
                        my.AccountName2 = ppp.AccountName2;
                        my.Ack = ppp.Ack;
                        my.DatePosted = ppp.DatePosted;
                        my.SettlementDate = ppp.SettlementDate;
                        my.Deal = ppp.Deal;
                        my.Price = ppp.Price;
                        my.Quantity = ppp.Quantity;
                        my.Security = ppp.Security;
                        my.SettlementDate = ppp.SettlementDate;
                        my.SecurityName = ppp.SecurityName;
                        db.DealerDGs.Add(my);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception f)
            {

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + f.Message);
                sw.Flush();
                sw.Close();
            }

        }
        public static Boolean isTaxable(string s)
        {
            Boolean test = false;
            string nm = "";
            var pace = db.Account_Creation.ToList().Where(a => a.CDSC_Number == s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm == "Taxable")
            {
                test = true;
            }
            return test;
        }

    }  
        }
    


