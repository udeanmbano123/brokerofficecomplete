//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerWin.DDO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Accounts_Documents
    {
        public string doc_generated { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
        public int Accounts_DocumentsID { get; set; }
    }
}
