﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrokerWin
{
    public class Accounts_Audit
    {
        [Key]
        public int ID { get; set; }
        public string CDS_Number { get; set; }
        public string BrokerCode { get; set; }
        public string AccountType { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string Forenames { get; set; }
        public string Initials { get; set; }
        public string Title { get; set; }
        public string IDNoPP { get; set; }
        public string IDtype { get; set; }
        public string Nationality { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Gender { get; set; }
        public string Add_1 { get; set; }
        public string Add_2 { get; set; }
        public string Add_3 { get; set; }
        public string Add_4 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Category { get; set; }
        public string Custodian { get; set; }
        public string TradingStatus { get; set; }
        public string Industry { get; set; }
        public string Tax { get; set; }
        public string Div_Bank { get; set; }
        public string Div_Branch { get; set; }
        public string Div_AccountNo { get; set; }
        public string Cash_Bank { get; set; }
        public string Cash_Branch { get; set; }
        public string Cash_AccountNo { get; set; }
        public byte[] Client_Image { get; set; }
        public byte[] Documents { get; set; }
        public byte[] BioMatrix { get; set; }
        public string Attached_Documents { get; set; }
        public Nullable<System.DateTime> Date_Created { get; set; }
        public string Update_Type { get; set; }
        public string Created_By { get; set; }
        public string AuthorizationState { get; set; }
        public string Comments { get; set; }
        public Nullable<decimal> VerificationCode { get; set; }
        public string DivPayee { get; set; }
        public string SettlementPayee { get; set; }
        public string idnopp2 { get; set; }
        public string idtype2 { get; set; }
        public string client_image2 { get; set; }
        public string documents2 { get; set; }
        public string isin { get; set; }
        public string company_code { get; set; }
        public string mobile_money { get; set; }
        public string mobile_number { get; set; }
        public string currency { get; set; }
        public string Indegnous { get; set; }
        public string Race { get; set; }
        public string Disadvantaged { get; set; }
        public string NationalityBy { get; set; }
        public string Custody { get; set; }
        public string Trading { get; set; }
        public string Source_Name { get; set; }
    }
    public partial class para_Billing
    {
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public Nullable<double> PercentageOrValue { get; set; }
        public string ApplyTo { get; set; }
        public string Indicator { get; set; }
        public int ID { get; set; }
    }


    public class OrderUpdatess
    {
        public string OrderNo { get; set; }
        public string OrderStatus { get; set; }

        public string trading_platform { get; set; }
    }
    public class Pre_Order_Live
    {
        public long OrderNo { get; set; }
        public string OrderType { get; set; }
        public string Company { get; set; }
        public string SecurityType { get; set; }
        public string CDS_AC_No { get; set; }
        public string Broker_Code { get; set; }
        public string Client_Type { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public string Shareholder { get; set; }
        public string ClientName { get; set; }
        public Nullable<int> TotalShareHolding { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<System.DateTime> Create_date { get; set; }
        public Nullable<System.DateTime> Deal_Begin_Date { get; set; }
        public Nullable<System.DateTime> Expiry_Date { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
        public Nullable<int> AvailableShares { get; set; }
        public string OrderPref { get; set; }
        public string OrderAttribute { get; set; }
        public string Marketboard { get; set; }
        public string TimeInForce { get; set; }
        public string OrderQualifier { get; set; }
        public string BrokerRef { get; set; }
        public string ContraBrokerId { get; set; }
        public Nullable<decimal> MaxPrice { get; set; }
        public Nullable<decimal> MiniPrice { get; set; }
        public Nullable<bool> Flag_oldorder { get; set; }
        public string OrderNumber { get; set; }
        public string Currency { get; set; }
        public Nullable<bool> FOK { get; set; }
        public Nullable<bool> Affirmation { get; set; }
        public string trading_platform { get; set; }
        public string Symbol { get; set; }
    }
    public class TR
    {
        [Column(TypeName = "money")]
        public Decimal Mo { get; set; }
    }
}
