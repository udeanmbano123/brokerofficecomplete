﻿using BrokerWin.DDO;
using BrokerWin.DealClass;
using BrokerWin.DealNotes;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BrokerWin
{
    public static class Library
    {
        public static BrokerOfficeEntities db = new BrokerOfficeEntities();
        public static void WriteErrorLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }

        }
        public static List<UplodedDealG> GetEarnerss()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Exchange,Market,Trader,Broker,BuySell,ShortSell,fileName,UploadedBy,trans,Symbol,client,price,sum(cast(Volume as float)) as Volume,ExecutionDateTime,CAST(ExecutionDateTime as date) as 'ExecutionDateTime2' from UplodedDeals where trans='0' and UploadedBy<>'EXCEPTION' group by Symbol,client,price,Exchange,Market,Trader,Broker,BuySell,ShortSell,fileName,UploadedBy,trans,ExecutionDateTime having count(*)>1";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<UplodedDealG>();
            while (reader.Read())
            {
                var accountDetails = new UplodedDealG
                {


                    Exchange = reader.GetValue(0).ToString(),
                    Market = reader.GetValue(1).ToString(),
                    Trader = reader.GetValue(2).ToString(),
                    Broker = reader.GetValue(3).ToString(),
                    BuySell = reader.GetValue(4).ToString(),
                    ShortSell = reader.GetValue(5).ToString(),
                    fileName = reader.GetValue(6).ToString(),
                    UploadedBy = reader.GetValue(7).ToString(),
                    trans = Convert.ToBoolean(reader.GetValue(8).ToString()),
                    Symbol = reader.GetValue(9).ToString(),
                    Client = reader.GetValue(10).ToString(),
                    Price = reader.GetValue(11).ToString(),
                    Volume = reader.GetValue(12).ToString(),
                    ExecutionDateTime = reader.GetValue(13).ToString(),
                    ExecutionDateTime2 = Convert.ToDateTime(reader.GetValue(14))

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        public static void importdealsG()
        {
            
            var c = GetEarnerss().ToList();

            foreach (var ppc in c)
            {
                try
                {//get maximum id from dealerG
                    var maxw = 0;

                    try
                    {
                        maxw = db.DealerDGs.ToList().Max(a => a.ID);

                    }
                    catch (Exception)
                    {

                        maxw = 0;
                    }
                    maxw = maxw + 1;
                    DealerDG2 dealerDG2 = new DealerDG2();
                    if (ppc.BuySell.ToLower() == ("Buy").ToLower())
                    {
                        dealerDG2.Account1 = ppc.Client;
                        dealerDG2.AccountName1 = GetNames(ppc.Client);

                    }
                    else
                    {
                        dealerDG2.Account1 = "ZSE";
                        dealerDG2.AccountName1 = "ZSE";

                    }

                    if (ppc.BuySell.ToLower() == ("Sell").ToLower())

                    {
                        dealerDG2.Account2 = ppc.Client;
                        dealerDG2.AccountName2 = GetNames(ppc.Client);

                    }
                    else
                    {
                        dealerDG2.Account2 = "ZSE";
                        dealerDG2.AccountName2 = "ZSE";

                    }
                    dealerDG2.Ack = "SETTLED";
                    dealerDG2.DatePosted = ppc.ExecutionDateTime2;
                    dealerDG2.PostDate = ppc.ExecutionDateTime2;
                    dealerDG2.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                    dealerDG2.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                    dealerDG2.Deal = maxw + "Z";
                    dealerDG2.OldDeal = maxw + "Z";
                    dealerDG2.Price = ppc.Price;
                    dealerDG2.Quantity = ppc.Volume;
                    dealerDG2.Security = ppc.Symbol;
                    dealerDG2.SecurityName = ppc.Symbol;
                    dealerDG2.Exchange = ppc.Exchange;
                    dealerDG2.Printed = false;
                    dealerDG2.Status = "None";
                    db.DealerDG2.Add(dealerDG2);
                    db.SaveChanges();

                    //update dealerDG
                    DealerDG pp = new DealerDG();
                    if (ppc.BuySell.ToLower() == ("Buy").ToLower())
                    {
                        pp.Account1 = ppc.Client;
                        pp.AccountName1 = GetNames(ppc.Client);

                    }
                    else
                    {
                        pp.Account1 = "ZSE";
                        pp.AccountName1 = "ZSE";

                    }

                    if (ppc.BuySell.ToLower() == ("Sell").ToLower())

                    {
                        pp.Account2 = ppc.Client;
                        pp.AccountName2 = GetNames(ppc.Client);

                    }
                    else
                    {
                        pp.Account2 = "ZSE";
                        pp.AccountName2 = "ZSE";

                    }
                    pp.Ack = "SETTLED";
                    pp.DatePosted = ppc.ExecutionDateTime;
                    pp.PostDate = ppc.ExecutionDateTime2;
                    pp.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                    pp.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                    pp.Deal = maxw + "Z";
                    pp.Price = ppc.Price;
                    pp.Quantity = ppc.Volume;
                    pp.Security = ppc.Symbol;
                    pp.SecurityName = ppc.Symbol;
                    pp.Exchange = ppc.Exchange;
                    pp.Printed = false;
                    pp.Status = "None";
                    db.DealerDGs.Add(pp);
                    db.SaveChanges();
                    //remove to transaction charges and trans

                    Tbl_MatchedDeals my = new Tbl_MatchedDeals();
                    my.Deal = pp.Deal;
                    my.BuyCompany = pp.Security;
                    my.SellCompany = pp.Security;
                    my.Buyercdsno = pp.Account1;
                    my.Sellercdsno = pp.Account2;
                    my.Quantity = Convert.ToDecimal(pp.Quantity);
                    my.Trade = Convert.ToDateTime(pp.DatePosted);
                    my.DealPrice = Convert.ToDecimal(pp.Price);
                    my.DealFlag = "C";
                    my.Instrument = "NULL";
                    my.Affirmation = false;
                    my.buybroker = pp.AccountName1;
                    my.sellbroker = pp.AccountName2;
                    try
                    {
                        my.RefID = Convert.ToInt32(0);
                    }
                    catch (Exception)
                    {

                        my.RefID = 0;
                    }
                    my.BrokerCode = "AKRI";
                    my.exchange = pp.Exchange;
                    db.Tbl_MatchedDeals.Add(my);
                    db.SaveChanges();


                    ////repost to transaction charges and trans
                    int? qty = Convert.ToInt32(dealerDG2.Quantity); int? qty2 = Convert.ToInt32(dealerDG2.Quantity);
                    decimal? buys = Convert.ToDecimal(dealerDG2.Price) * qty; decimal? sells = Convert.ToDecimal(dealerDG2.Price) * qty2;
                    string buyc = dealerDG2.AccountName1; string sellc = dealerDG2.AccountName2;
                    string buyer = dealerDG2.Account1;
                    string seller = dealerDG2.Account2;
                    string sec = dealerDG2.Security;
                    string sec2 = dealerDG2.Security;
                    decimal? s = 0;

                    try
                    {
                        s = (decimal.Parse(sells.ToString(), CultureInfo.InvariantCulture));

                    }
                    catch (Exception)
                    {

                        s = 0;
                    }
                    decimal? b = s;
                    var p = db.TradingCharges.ToList();
                    foreach (var z in p)
                    {
                        BrokerWin.DDO.TransactionCharge myyc = new TransactionCharge();

                        myyc.Transcode = dealerDG2.Deal;
                        myyc.ChargeCode = z.chargecode;
                        if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                        {

                            myyc.BuyCharges = 0;
                        }
                        else
                        {
                            if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {
                                myyc.BuyCharges = 0;
                            }
                            else
                            {
                                myyc.BuyCharges = b * Convert.ToDecimal((z.chargevalue / 100));

                            }



                        }
                        if (z.chargecode.ToLower().Replace(" ", "") == ("Stamp duty").ToLower().Replace(" ", ""))
                        {
                            myyc.SellCharges = 0;

                        }
                        else
                        {
                            if (isTaxable2(seller) == false && z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {
                                myyc.SellCharges = 0;
                            }
                            else
                            {

                                myyc.SellCharges = b * Convert.ToDecimal((z.chargevalue / 100));
                            }
                        }
                        string dgg = qty + " " + sec + " @ " + b;
                        myyc.Date = dealerDG2.DatePosted;
                        postTransCharge(myyc.BuyCharges, myyc.SellCharges, myyc.ChargeCode, myyc.Transcode, myyc.Date.ToString(), dgg);


                        db.TransactionCharges.Add(myyc);
                        db.SaveChanges();
                    }
                    //trans
                    //Post to accounts
                    string desc = qty + " " + sec + " @ " + b;
                    decimal? total = 0;
                    decimal? total2 = 0;
                    //insert charge to Trans
                    var zzz = db.TransactionCharges.ToList().Where(a => a.Transcode == maxw + "Z");
                    foreach (var f in zzz)
                    {

                        total += f.BuyCharges;


                        total2 += f.SellCharges;




                    }
                    string buy = "";
                    string buyaacount = "";
                    string sell = "";
                    string sellacount = "";
                    string buyA = "";
                    string buyaacountA = "";
                    string sellA = "";
                    string sellacountA = "";
                    var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
                    foreach (var ppca in pk)
                    {
                        buy = ppca.AccountName;
                        buyaacount = ppca.AccountNumber;
                    }
                    var tk = db.WindowsServices.ToList().Where(a => a.Action == "SELL");
                    foreach (var u in tk)
                    {
                        sell = u.AccountName;
                        sellacount = u.AccountNumber;
                    }
                    var pkA = db.WindowsServices.ToList().Where(a => a.Action == "BUYA");
                    foreach (var ppca in pkA)
                    {
                        buyA = ppca.AccountName;
                        buyaacountA = ppca.AccountNumber;
                    }
                    var tkA = db.WindowsServices.ToList().Where(a => a.Action == "SELLA");
                    foreach (var u in tkA)
                    {
                        sellA = u.AccountName;
                        sellacountA = u.AccountNumber;
                    }

                    decimal? deals = 0;
                    decimal? bb = 0;
                    decimal? ss = 0;

                    ////deals = Convert.ToDecimal((Math.Round(Convert.ToDouble(ppp.Price), 4) * Math.Round(Convert.ToDouble(ppp.Quantity), 4)));
                    deals = b;
                    bb = b + total;
                    ss = b - total2;
                    buys = Convert.ToDecimal(ppc.Price);
                    //WriteErrorLog(b.ToString() + " :");

                    //Tran my2 = new Tran();
                    ////debit account number

                    //my2.Account = dealerDG2.Account1;
                    //my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my2.Credit = 0;
                    //my2.Debit = bb;
                    //my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my2.Reference_Number = dealerDG2.Deal;
                    //my2.TrxnDate = dealerDG2.DatePosted;
                    //my2.Post = dealerDG2.DatePosted;
                    //my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                    //db.Trans.Add(my2);
                    ////Update table  
                    //db.SaveChanges();
                    //////update Acc Receivable
                    //Tran my22 = new Tran();
                    //my22.Account = buyaacount;
                    //my22.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my22.Credit = 0;
                    //my22.Debit = bb;
                    //my22.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my22.Reference_Number = dealerDG2.Deal;
                    //my22.TrxnDate = dealerDG2.DatePosted;
                    //my22.Post = dealerDG2.DatePosted;
                    //my22.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                    //my22.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                    //db.Trans.Add(my22);
                    //db.SaveChanges();

                    //Tran my4 = new Tran();

                    //////debit account number

                    //my4.Account = dealerDG2.Account2;

                    //my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                    //my4.Credit = ss;
                    //my4.Debit = 0;
                    //my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                    //my4.Reference_Number = dealerDG2.Deal;
                    //my4.TrxnDate = dealerDG2.DatePosted;
                    //my4.Post = dealerDG2.DatePosted;
                    //// my2.Type = "Sell Deal";
                    //my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                    //my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                    //db.Trans.Add(my4);
                    //db.SaveChanges();
                    //////accounts payable
                    //////debit account number
                    //Tran my5 = new Tran();
                    //my5.Account = sellacount;
                    //my5.Category = "Sell " + qty + " " + sec + " @ " + buys;
                    //my5.Credit = ss;
                    //my5.Debit = 0;
                    //my5.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                    //my5.Reference_Number = dealerDG2.Deal;
                    //my5.TrxnDate = dealerDG2.DatePosted;
                    //my5.Post = dealerDG2.DatePosted;
                    //my5.Type = "Sell " + qty + " " + sec + " @ " + buys;
                    //my5.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                    //db.Trans.Add(my5);
                    //db.SaveChanges();
                    if (chkclient(buyer) == true)
                    {
                        Tran my2 = new Tran();


                        //debit account number

                        my2.Account = NNull(buyer);


                        my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Credit = 0;
                        my2.Debit = Buy(dealerDG2.Deal);
                        my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Reference_Number = dealerDG2.Deal;
                        my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                        db.Trans.Add(my2);

                        //Update table  
                        db.SaveChanges();

                        my2.Account = buyaacount;
                        my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Credit = 0;
                        my2.Debit = Buy(dealerDG2.Deal);
                        my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Reference_Number = dealerDG2.Deal;
                        my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                        db.Trans.Add(my2);


                        db.SaveChanges();
                        //credit bramount
                        my2.Account = buyaacountA;
                        my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Credit = BR(dealerDG2.Deal);
                        my2.Debit = 0;
                        my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.Reference_Number = dealerDG2.Deal;
                        my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                        my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                        db.Trans.Add(my2);


                        db.SaveChanges();
                    }

                    //oppose buyaccount

                    //debit account number
                    if (chkclient(seller) == true)
                    {
                        Tran my4 = new Tran();

                        //debit account number

                        my4.Account = NNull(seller);

                        if (my4.Account == "")
                        {
                            my4.Account = "ZSE";
                        }
                        my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Credit = Sell(dealerDG2.Deal);
                        my4.Debit = 0;
                        my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Reference_Number = dealerDG2.Deal;
                        my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        // my2.Type = "Sell Deal";
                        my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                        db.Trans.Add(my4);
                        db.SaveChanges();
                        ////accounts payable

                        my4.Account = sellacount;
                        my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Credit = Sell(dealerDG2.Deal);
                        my4.Debit = 0;
                        my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Reference_Number = dealerDG2.Deal;
                        my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        // my2.Type = "Sell Deal";
                        my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                        db.Trans.Add(my4);
                        db.SaveChanges();
                        //credit bramount

                        my4.Account = sellacountA;
                        my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Credit = 0;
                        my4.Debit = BR(dealerDG2.Deal);
                        my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.Reference_Number = dealerDG2.Deal;
                        my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                        my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                        // my2.Type = "Sell Deal";
                        my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                        my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                        db.Trans.Add(my4);
                        db.SaveChanges();


                    }


                    //flag deals
                    var pss = flagdeaals(ppc.Client, ppc.Price, ppc.Symbol, ppc.fileName);

                    foreach (var z in pss)
                    {
                        var myp = db.UplodedDeals.Find(z.UplodedDealsID);
                        myp.trans = true;
                        db.UplodedDeals.AddOrUpdate(myp);
                        db.SaveChanges();

                    }

                }
                catch (Exception ex)
                {
                    WriteErrorLog(ex.ToString());
                    continue;
                }
            }
        }

        public static string NNull(string s)
        {
            if (s == null || s == "")
            {
                s = "ZSE";
            }

            return s;
        }

        public static bool chkclient(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.Account_Creation.ToList().Where(a => a.CDSC_Number == m).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (m.ToLower() == "zse")
            {
                tru = false;
            }
            if (m.ToLower() == "finsec")
            {
                tru = false;
            }

            return tru;

        }

        public static List<UplodedDealM> flagdeaals(string c, string p, string s, string f)
        {
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select a.UplodedDealsID from UplodedDeals a JOIN  (select Symbol, client, price, sum(cast(Volume as float)) as TotVolume from UplodedDeals where Client ='" + c + "' and Price ='" + p + "' and Symbol='" + s + "' and fileName='" + f + "' and UploadedBy<>'EXCEPTION' group by Symbol, client, price having count(*) > 1) tblGroup ON a.Symbol = tblGroup.Symbol and a.Client = tblGroup.Client and a.Price = tblGroup.Price order by a.Client";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<UplodedDealM>();
            while (reader.Read())
            {
                var accountDetails = new UplodedDealM
                {


                    UplodedDealsID = Convert.ToInt32(reader.GetValue(0).ToString()),

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            return accDetails;
        }


        public static void importdeals()
        {
            var c = (from y in db.UplodedDeals
                    where  y.UploadedBy!="EXCEPTION"
                    select y).ToList();

            foreach (var ppc in c)
                {

                    if ((deal(ppc.TicketNumber) == false) && ppc.trans==false)
                    {
                 
                        try
                        {

                            DealerDG2 dealerDG2 = new DealerDG2();
                            if (ppc.BuySell.ToLower() == ("Buy").ToLower())
                            {
                                dealerDG2.Account1 = ppc.Client;
                                dealerDG2.AccountName1 = GetNames(ppc.Client);

                            }
                            else
                            {
                                dealerDG2.Account1 = "ZSE";
                                dealerDG2.AccountName1 = "ZSE";

                            }

                            if (ppc.BuySell.ToLower() == ("Sell").ToLower())

                            {
                                dealerDG2.Account2 = ppc.Client;
                                dealerDG2.AccountName2 = GetNames(ppc.Client);

                            }
                            else
                            {
                                dealerDG2.Account2 = "ZSE";
                                dealerDG2.AccountName2 = "ZSE";

                            }
                            dealerDG2.Ack = "SETTLED";
                        dealerDG2.DatePosted = ppc.ExecutionDateTime;
                        dealerDG2.PostDate = ppc.ExecutionDateTime;
                        dealerDG2.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                        dealerDG2.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                        dealerDG2.Deal = ppc.TicketNumber;
                            dealerDG2.OldDeal = ppc.TicketNumber;
                            dealerDG2.Price = ppc.Price;
                            dealerDG2.Quantity = ppc.Volume;
                            dealerDG2.Security = ppc.Symbol;
                            dealerDG2.SecurityName = ppc.Symbol;
                            dealerDG2.Exchange = ppc.Exchange;
                            dealerDG2.Printed = false;
                            dealerDG2.Status = "None";
                            db.DealerDG2.Add(dealerDG2);
                            db.SaveChanges();

                            //update dealerDG
                            DealerDG pp = new DealerDG();
                            if (ppc.BuySell.ToLower() == ("Buy").ToLower())
                            {
                                pp.Account1 = ppc.Client;
                                pp.AccountName1 = GetNames(ppc.Client);

                            }
                            else
                            {
                                pp.Account1 = "ZSE";
                                pp.AccountName1 = "ZSE";

                            }

                            if (ppc.BuySell.ToLower() == ("Sell").ToLower())

                            {
                                pp.Account2 = ppc.Client;
                                pp.AccountName2 = GetNames(ppc.Client);

                            }
                            else
                            {
                                pp.Account2 = "ZSE";
                                pp.AccountName2 = "ZSE";

                            }
                            pp.Ack = "SETTLED";
                        pp.DatePosted = ppc.ExecutionDateTime.ToString();
                        pp.PostDate = ppc.ExecutionDateTime;
                        pp.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                        pp.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                        pp.Deal = ppc.TicketNumber;
                            pp.Price = ppc.Price;
                            pp.Quantity = ppc.Volume;
                            pp.Security = ppc.Symbol;
                            pp.SecurityName = ppc.Symbol;
                            pp.Exchange = ppc.Exchange;
                            pp.Printed = false;
                            pp.Status = "None";
                            db.DealerDGs.Add(pp);
                            db.SaveChanges();
                            //remove to transaction charges and trans

                            Tbl_MatchedDeals my = new Tbl_MatchedDeals();
                            my.Deal = pp.Deal;
                            my.BuyCompany = pp.Security;
                            my.SellCompany = pp.Security;
                            my.Buyercdsno = pp.Account1;
                            my.Sellercdsno = pp.Account2;
                            my.Quantity = Convert.ToDecimal(pp.Quantity);
                            my.Trade = Convert.ToDateTime(pp.DatePosted);
                            my.DealPrice = Convert.ToDecimal(pp.Price);
                            my.DealFlag = "C";
                            my.Instrument = "NULL";
                            my.Affirmation = false;
                            my.buybroker = pp.AccountName1;
                            my.sellbroker = pp.AccountName2;
                            try
                            {
                                my.RefID = Convert.ToInt32(ppc.OrderNumber);
                            }
                            catch (Exception)
                            {

                                my.RefID = 0;
                            }
                            my.BrokerCode = "AKRI";
                            my.exchange = pp.Exchange;
                            db.Tbl_MatchedDeals.Add(my);
                            db.SaveChanges();


                            ////repost to transaction charges and trans
                            int? qty = Convert.ToInt32(dealerDG2.Quantity); int? qty2 = Convert.ToInt32(dealerDG2.Quantity);
                            decimal? buys = Convert.ToDecimal(dealerDG2.Price) * qty; decimal? sells = Convert.ToDecimal(dealerDG2.Price) * qty2;
                            string buyc = dealerDG2.AccountName1; string sellc = dealerDG2.AccountName2;
                            string buyer = dealerDG2.Account1;
                            string seller = dealerDG2.Account2;
                            string sec = dealerDG2.Security;
                            string sec2 = dealerDG2.Security;
                            decimal? s = 0;

                            try
                            {
                                s = (decimal.Parse(sells.ToString(), CultureInfo.InvariantCulture));

                            }
                            catch (Exception)
                            {

                                s = 0;
                            }
                            decimal? b = s;
                            var p = db.TradingCharges.ToList();
                            foreach (var z in p)
                            {
                                BrokerWin.DDO.TransactionCharge myyc = new TransactionCharge();

                                myyc.Transcode = dealerDG2.Deal;
                                myyc.ChargeCode = z.chargecode;
                                if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                {

                                    myyc.BuyCharges = 0;
                                }
                                else
                                {
                                    if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                    {
                                        myyc.BuyCharges = 0;
                                    }
                                    else
                                    {
                                        myyc.BuyCharges = b * Convert.ToDecimal((z.chargevalue / 100));

                                    }



                                }
                                if (z.chargecode.ToLower().Replace(" ", "") == ("Stamp duty").ToLower().Replace(" ", ""))
                                {
                                    myyc.SellCharges = 0;

                                }
                                else
                                {
                                    if (isTaxable2(seller) == false && z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                    {
                                        myyc.SellCharges = 0;
                                    }
                                    else
                                    {

                                        myyc.SellCharges = b * Convert.ToDecimal((z.chargevalue / 100));
                                    }
                                }
                                string dgg = qty + " " + sec + " @ " + b;
                                myyc.Date = dealerDG2.DatePosted;
                                postTransCharge(myyc.BuyCharges, myyc.SellCharges, myyc.ChargeCode, myyc.Transcode, myyc.Date.ToString(), dgg);


                                db.TransactionCharges.Add(myyc);
                                db.SaveChanges();
                            }
                            //trans
                            //Post to accounts
                            string desc = qty + " " + sec + " @ " + b;
                            decimal? total = 0;
                            decimal? total2 = 0;
                            //insert charge to Trans
                            var zzz = db.TransactionCharges.ToList().Where(a => a.Transcode == ppc.TicketNumber);
                            foreach (var f in zzz)
                            {

                                total += f.BuyCharges;


                                total2 += f.SellCharges;




                            }
                        string buy = "";
                        string buyaacount = "";
                        string sell = "";
                        string sellacount = "";
                        string buyA = "";
                        string buyaacountA = "";
                        string sellA = "";
                        string sellacountA = "";
                        var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
                        foreach (var ppca in pk)
                        {
                            buy = ppca.AccountName;
                            buyaacount = ppca.AccountNumber;
                        }
                        var tk = db.WindowsServices.ToList().Where(a => a.Action == "SELL");
                        foreach (var u in tk)
                        {
                            sell = u.AccountName;
                            sellacount = u.AccountNumber;
                        }
                        var pkA = db.WindowsServices.ToList().Where(a => a.Action == "BUYA");
                        foreach (var ppca in pkA)
                        {
                            buyA = ppca.AccountName;
                            buyaacountA = ppca.AccountNumber;
                        }
                        var tkA = db.WindowsServices.ToList().Where(a => a.Action == "SELLA");
                        foreach (var u in tkA)
                        {
                            sellA = u.AccountName;
                            sellacountA = u.AccountNumber;
                        }

                        decimal? deals = 0;
                            decimal? bb = 0;
                            decimal? ss = 0;

                            ////deals = Convert.ToDecimal((Math.Round(Convert.ToDouble(ppp.Price), 4) * Math.Round(Convert.ToDouble(ppp.Quantity), 4)));
                            deals = b;
                            bb = b + total;
                            ss = b - total2;
                            buys = Convert.ToDecimal(ppc.Price);
                        //WriteErrorLog(b.ToString() + " :");

                        //Tran my2 = new Tran();
                        ////debit account number

                        //my2.Account = dealerDG2.Account1;
                        //my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my2.Credit = 0;
                        //my2.Debit = bb;
                        //my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my2.Reference_Number = dealerDG2.Deal;
                        //my2.TrxnDate = dealerDG2.DatePosted;
                        //my2.Post = dealerDG2.DatePosted;
                        //my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                        //db.Trans.Add(my2);
                        ////Update table  
                        //db.SaveChanges();
                        //////update Acc Receivable
                        //Tran my22 = new Tran();
                        //my22.Account = buyaacount;
                        //my22.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my22.Credit = 0;
                        //my22.Debit = bb;
                        //my22.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my22.Reference_Number = dealerDG2.Deal;
                        //my22.TrxnDate = dealerDG2.DatePosted;
                        //my22.Post = dealerDG2.DatePosted;
                        //my22.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                        //my22.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                        //db.Trans.Add(my22);
                        //db.SaveChanges();

                        //Tran my4 = new Tran();

                        //////debit account number

                        //my4.Account = dealerDG2.Account2;

                        //my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                        //my4.Credit = ss;
                        //my4.Debit = 0;
                        //my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                        //my4.Reference_Number = dealerDG2.Deal;
                        //my4.TrxnDate = dealerDG2.DatePosted;
                        //my4.Post = dealerDG2.DatePosted;
                        //// my2.Type = "Sell Deal";
                        //my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                        //my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                        //db.Trans.Add(my4);
                        //db.SaveChanges();
                        //////accounts payable
                        //////debit account number
                        //Tran my5 = new Tran();
                        //my5.Account = sellacount;
                        //my5.Category = "Sell " + qty + " " + sec + " @ " + buys;
                        //my5.Credit = ss;
                        //my5.Debit = 0;
                        //my5.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                        //my5.Reference_Number = dealerDG2.Deal;
                        //my5.TrxnDate = dealerDG2.DatePosted;
                        //my5.Post = dealerDG2.DatePosted;
                        //my5.Type = "Sell " + qty + " " + sec + " @ " + buys;
                        //my5.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                        //db.Trans.Add(my5);
                        //db.SaveChanges();
                        if (chkclient(buyer) == true)
                        {
                            Tran my2 = new Tran();


                            //debit account number

                            my2.Account = NNull(buyer);


                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(dealerDG2.Deal);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = dealerDG2.Deal;
                            my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);

                            //Update table  
                            db.SaveChanges();

                            my2.Account = buyaacount;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(dealerDG2.Deal);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = dealerDG2.Deal;
                            my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);


                            db.SaveChanges();
                            //credit bramount
                            my2.Account = buyaacountA;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = BR(dealerDG2.Deal);
                            my2.Debit = 0;
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = dealerDG2.Deal;
                            my2.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Trans.Add(my2);


                            db.SaveChanges();
                        }

                        //oppose buyaccount

                        //debit account number
                        if (chkclient(seller) == true)
                        {
                            Tran my4 = new Tran();

                            //debit account number

                            my4.Account = NNull(seller);

                            if (my4.Account == "")
                            {
                                my4.Account = "ZSE";
                            }
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(dealerDG2.Deal);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = dealerDG2.Deal;
                            my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();
                            ////accounts payable

                            my4.Account = sellacount;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(dealerDG2.Deal);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = dealerDG2.Deal;
                            my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();
                            //credit bramount

                            my4.Account = sellacountA;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = 0;
                            my4.Debit = BR(dealerDG2.Deal);
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = dealerDG2.Deal;
                            my4.TrxnDate = Convert.ToDateTime(dealerDG2.PostDate);
                            my4.Post = Convert.ToDateTime(dealerDG2.PostDate);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Trans.Add(my4);
                            db.SaveChanges();


                        }


                        ppc.trans = true;
                            var zz = db.UplodedDeals.Find(ppc.UplodedDealsID);
                            zz.trans = true;
                            db.UplodedDeals.AddOrUpdate(zz);

                            db.SaveChanges();
                        }

                        catch (Exception f)
                        {


                            WriteErrorLog(f.Message);
                        }

                    }
                }

            
            
        }

        

        public static bool deal(string s)
        {
           bool two = false;
            var p = 0;
            try
            {
              p = db.DealerDGs.ToList().Where(a => a.Deal == s).Count();

            }
            catch (Exception)
            {

                p = 0;
            }

            if (p > 0)
            {
                two = true;
            }
            else
            {
                two = false;
            }


            return two;
        }
        public static DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        public static string GetNames(string acc)
        {
            var c = db.Account_Creation.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }
        public static bool isTaxable2(string s)
        {
            bool test = false;
            string nm = "";
            var pace = db.Account_Creation.ToList().Where(a => a.CDSC_Number == s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm == "Taxable")
            {
                test = true;
            }
            return test;
        }
        public static void postTransCharge2(decimal? p, decimal? k, string c, string transcode, string st, string vv)
        {
            string buy = "";
            string buyaacount = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");

            foreach (var t in pk)
            {
                buy = t.AccountName;
                buyaacount = t.AccountNumber;
            }

            var dz = db.TradingCharges.ToList().Where(a => a.ChargeName.ToLower().Replace(" ", "") == c.ToLower().Replace(" ", ""));
            double pin = Math.Round(Convert.ToDouble((p + k)), 4);
            foreach (var q in dz)
            {

                Tran my3 = new Tran();

                my3.Account = q.chargeaccountcode.ToString();
                my3.Category = "Order Posting";
                my3.Credit = p + k;
                my3.Debit = 0;
                my3.Narration = vv;
                my3.Reference_Number = transcode;
                my3.TrxnDate = Convert.ToDateTime(st);
                my3.Post = Convert.ToDateTime(st);
                my3.Type = q.chartaccount;
                my3.PostedBy = "Background";
                db.Trans.Add(my3);
                db.SaveChanges();
            }


        }

        public static void trades2()
        {


            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Matcheds", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            List<Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<Tbl_MatchedDeals>>(validate);


            var dbsel = from s in dataList
                        join v in db.Account_Creation on s.Buyercdsno equals v.CDSC_Number
                        let SID = s.ID
                        let Deal = s.Deal
                        let BuyCompany = s.BuyCompany
                        let SellCompany = s.SellCompany
                        let Buyer = s.Buyercdsno
                        let Seller = s.Sellercdsno
                        let Quantity = s.Quantity
                        let Trade = s.Trade
                        let DealPrice = s.DealPrice
                        let DealFlag = s.DealFlag
                        let Instrument = s.Instrument
                        let affirmation = s.Affirmation
                        let buybroker = s.buybroker
                        let sellbroker = s.sellbroker
                        select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };
            var dbsel2 = from s in dataList
                         join v in db.Account_Creation on s.Sellercdsno equals v.CDSC_Number
                         let SID = s.ID
                         let Deal = s.Deal
                         let BuyCompany = s.BuyCompany
                         let SellCompany = s.SellCompany
                         let Buyer = s.Buyercdsno
                         let Seller = s.Sellercdsno
                         let Quantity = s.Quantity
                         let Trade = s.Trade
                         let DealPrice = s.DealPrice
                         let DealFlag = s.DealFlag
                         let Instrument = s.Instrument
                         let affirmation = s.Affirmation
                         let buybroker = s.buybroker
                         let sellbroker = s.sellbroker
                         select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };


            int my44 = dbsel.ToList().Count();
            int my45 = dbsel2.ToList().Count();

            var bag = dbsel.ToList();
            var bag2 = dbsel2.ToList();
            int sel = 0;
            foreach (var c in bag)
            {
                var ss = 0;

                try
                {
                    ss = db.Tbl_MatchedDeals.ToList().Where(a => a.RefID == c.SID).Count();


                }
                catch (Exception)
                {

                    ss = 0;
                }
                if (ss == 0)
                {

                    try
                    {
                        sel = db.Account_Creation.ToList().Where(a => a.CDSC_Number == c.Seller).Count();


                    }
                    catch (Exception)
                    {

                        sel = 0;
                    }
                    if (sel == 0)
                    {


                        Tbl_MatchedDeals my = new Tbl_MatchedDeals();
                        my.Deal = c.Deal;
                        my.BuyCompany = c.BuyCompany;
                        my.SellCompany = c.SellCompany;
                        my.Buyercdsno = c.Buyer;
                        my.Sellercdsno = c.Seller;
                        my.Quantity = c.Quantity;
                        my.Trade = c.Trade;
                        my.DealPrice = c.DealPrice;
                        my.DealFlag = c.DealFlag;
                        my.Instrument = c.Instrument;
                        my.Affirmation = c.affirmation;
                        my.buybroker = c.buybroker;
                        my.sellbroker = c.sellbroker;
                        my.RefID = Convert.ToInt32(c.SID);
                        my.BrokerCode = c.buybroker;
                        my.exchange = "FINSEC";
                        db.Tbl_MatchedDeals.Add(my);
                        db.SaveChanges();
                    }

                }

             

            }


            //update all sell

            foreach (var c in bag2)
            {
                var ss = 0;
                try
                {
                    ss = db.Tbl_MatchedDeals.ToList().Where(a => a.RefID == c.SID).Count();


                }
                catch (Exception)
                {

                    ss = 0;
                }
                if (ss == 0)
                {
                    Tbl_MatchedDeals my = new Tbl_MatchedDeals();
                    my.Deal = c.Deal;
                    my.BuyCompany = c.BuyCompany;
                    my.SellCompany = c.SellCompany;
                    my.Buyercdsno = c.Buyer;
                    my.Sellercdsno = c.Seller;
                    my.Quantity = c.Quantity;
                    my.Trade = c.Trade;
                    my.DealPrice = c.DealPrice;
                    my.DealFlag = c.DealFlag;
                    my.Instrument = c.Instrument;
                    my.Affirmation = c.affirmation;
                    my.buybroker = c.buybroker;
                    my.sellbroker = c.sellbroker;
                    my.RefID = Convert.ToInt32(c.SID);
                    my.BrokerCode = c.sellbroker;
                    my.exchange = "FINSEC";
                    db.Tbl_MatchedDeals.Add(my);
                    db.SaveChanges();
                }
            }


            //Update Names
            var ca = db.Tbl_MatchedDeals.ToList().Where(a=>a.SellerName==null || a.BuyerName==null);

            foreach (var p in ca)
            {
                Tbl_MatchedDeals my = db.Tbl_MatchedDeals.Find(p.ID);
                my.BuyerName = myNames(my.Buyercdsno);
                my.SellerName = myNames(my.Sellercdsno);
                db.Tbl_MatchedDeals.AddOrUpdate(my);
                db.SaveChanges();
            }

        }

        public static string myNames(string s)
        {
            string validate = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("GetName/{s}", Method.POST);
           request.AddUrlSegment("s",s);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();
           

            return validate;
        }
        public static void reoveaccount(string s)
        {
            int my = 0;
            var c = db.Account_Creation.ToList().Where(a => a.CDSC_Number == s);
            foreach (var p in c)
            {
                my = p.ID_;
            }

            try
            {
                Account_Creation account_Creation = db.Account_Creation.Find(my);
                account_Creation.StatuSActive = true;
                db.Account_Creation.AddOrUpdate(account_Creation);
                db.SaveChangesAsync();
            }
            catch (Exception)
            {


            }
        }

        public static void accounts()
        {
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("denied/{s}", Method.GET);
            request.AddUrlSegment("s", "AKRI");
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);


            var pers = from s in dataList
                       select s;
            string zee = "";
            foreach (var d in pers)
            {
                reoveaccount(d.CDS_Number);

            }
        }
        public static void updatebilling()
        {
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Billing", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<para_Billing> dataList = JsonConvert.DeserializeObject<List<para_Billing>>(validate);


            var pers = from s in dataList
                       select s;

            foreach (var k in pers)
            {
                List<TradingCharge> p = (from s in db.TradingCharges
                                          where s.ChargeName == k.ChargeName
                                          select s).ToList();

                foreach (TradingCharge c in p)
                {
                    TradingCharge my = db.TradingCharges.Find(c.TradingChargesID);
                    c.chargevalue = k.PercentageOrValue;
                    c.chargecode = k.ChargeCode;
                    if (k.ApplyTo == "BOTH")
                    {
                        c.tradeside = "Both";
                    }
                    else if (k.ApplyTo == "BUYER")
                    {
                        c.tradeside = "Buy";
                    }
                    else if (k.ApplyTo == "SELLER")
                    {
                        c.tradeside = "Sell";
                    }
                    if (k.Indicator == "PERCENT")
                    {
                        c.ChargedAs = "Percentage";
                    }
                    else if (k.Indicator == "FLAT")
                    {
                        c.ChargedAs = "Flat";
                    }
                    db.TradingCharges.AddOrUpdate(my);
                    db.SaveChanges();
                }
            }

        }
        public static void Orders()
        {
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("OrderUpdates/{f}", Method.GET);
            request.AddUrlSegment("f", "AKRI");
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<OrderUpdatess> dataList = JsonConvert.DeserializeObject<List<OrderUpdatess>>(validate);

            foreach (var p in dataList)
            {

                var pp = db.Order_Live.ToList().Where(a => a.OrderNumber == p.OrderNo && a.TP == p.trading_platform);
                foreach (var z in pp)
                {
                    var my = db.Order_Live.Find(z.OrderNo);
                    my.WebServiceID = p.OrderStatus;
                    db.Order_Live.AddOrUpdate(my);
                    db.SaveChanges();
                }
            }

        }
        public static void PostCharges()
        {
            try
            {
                importdealsG();
            }
            catch (Exception)
            {

                
            }
            try
            {
                importdeals();
            }
            catch (Exception)
            {

            }
            try
            {
                accounts();
            }
            catch (Exception)
            {

             
            }
            try
            {
                updatebilling();
            }
            catch (Exception)
            {

            
            }
        }

        public static string Setter(string s)
        {
            var dz = db.TradingCharges.ToList().Where(a => a.ChargeName.ToLower().Replace(" ","") == s.ToLower().Replace(" ", ""));
            string z = "";
            foreach (var p in dz)
            {
                z = p.tradeside;
            }

            return z;
        }

        public static List<TransactionCharge> GetEarners()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from [BrokerOffice].[dbo].[TransactionCharges]";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<TransactionCharge>();
            while (reader.Read())
            {
                var accountDetails = new TransactionCharge
                {

                    Id = Convert.ToInt32(reader.GetValue(0).ToString()),
                    ChargeCode = reader.GetValue(1).ToString(),

                    BuyCharges = Convert.ToDecimal(reader.GetValue(2).ToString()),
                    SellCharges = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    Date = Convert.ToDateTime(reader.GetValue(4).ToString()),
                    Transcode = reader.GetValue(5).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public static void postTransCharge(decimal? p, decimal? k, string c, string transcode, string st,string vv)
        {
            string buy = "";
            string buyaacount = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
            foreach (var t in pk)
            {
                buy = t.AccountName;
                buyaacount = t.AccountNumber;
            }

            var dz = db.TradingCharges.ToList().Where(a => a.ChargeName.ToLower().Replace(" ","") ==c.ToLower().Replace(" ", ""));
           
            foreach (var q in dz)
            {
                
                    Tran my3 = new Tran();

                    my3.Account = q.chargeaccountcode.ToString();
                    my3.Category = "Order Posting";
                    my3.Credit = p+k;
                    my3.Debit = 0;
                    my3.Narration = vv;
                    my3.Reference_Number = transcode;
                    my3.TrxnDate = Convert.ToDateTime(st);
                    my3.Post = Convert.ToDateTime(st);
                    my3.Type = q.chartaccount;
                    my3.PostedBy = "Background Service";
                    db.Trans.Add(my3);
                    db.SaveChanges();
                    //debit account
                    //my3.Account = buyaacount;
                    //my3.Category = "Order Posting";
                    //my3.Debit= Convert.ToDecimal(p + k);
                    //my3.Credit = 0;
                    //my3.Narration =vv;
                    //my3.Reference_Number = transcode;
                    //my3.TrxnDate = Convert.ToDateTime(st);
                    //my3.Post = Convert.ToDateTime(st);
                    //my3.Type = buy;
                    //my3.PostedBy = "Background Service";
                    //db.Trans.Add(my3);
                    //db.SaveChanges();

                //}
                //else if (Setter(q.ChargeName) == "Buy")
                //{
                //    Tran my3 = new Tran();

                //    my3.Account = q.chargeaccountcode.ToString();
                //    my3.Category = "Order Posting";
                //    my3.Credit = Convert.ToDecimal(p);
                //    my3.Debit = 0;
                //    my3.Narration =vv;
                //    my3.Reference_Number = transcode;
                //    my3.TrxnDate = Convert.ToDateTime(st);
                //    my3.Post = Convert.ToDateTime(st);
                //    my3.Type = q.chartaccount;
                //    my3.PostedBy = "Background Service";
                //    db.Trans.Add(my3);
                //    db.SaveChanges();
                //    //debit account
                //    //my3.Account = buyaacount;
                //    //my3.Category = "Order Posting";
                //    //my3.Debit = Convert.ToDecimal(p);
                //    //my3.Credit = 0;
                //    //my3.Narration = vv;
                //    //my3.Reference_Number = transcode;
                //    //my3.TrxnDate = Convert.ToDateTime(st);
                //    //my3.Post = Convert.ToDateTime(st);
                //    //my3.Type = buy;
                //    //my3.PostedBy = "Background Service";
                //    //db.Trans.Add(my3);
                //    //db.SaveChanges();

                //}
                //else if (Setter(q.ChargeName) == "Sell")
                //{
                //    Tran my3 = new Tran();

                //    my3.Account = q.chargeaccountcode.ToString();
                //    my3.Category = "Order Posting";
                //    my3.Credit = Convert.ToDecimal(k);
                //    my3.Debit = 0;
                //    my3.Narration = vv;
                //    my3.Reference_Number = transcode;
                //    my3.TrxnDate = Convert.ToDateTime(st);
                //    my3.Post = Convert.ToDateTime(st);
                //    my3.Type = q.chartaccount;
                //    my3.PostedBy = "Background Service";
                //    db.Trans.Add(my3);
                //    db.SaveChanges();
                //    //
                //    //my3.Account = buyaacount;
                //    //my3.Category = "Order Posting";
                //    //my3.Debit = Convert.ToDecimal(k);
                //    //my3.Credit = 0;
                //    //my3.Narration = vv;
                //    //my3.Reference_Number = transcode;
                //    //my3.TrxnDate = Convert.ToDateTime(st);
                //    //my3.Post = Convert.ToDateTime(st);
                //    //my3.Type = buy;
                //    //my3.PostedBy = "Background Service";
                //    //db.Trans.Add(my3);
                //    //db.SaveChanges();



                //}


            }
        }

        public static void AddDeals()
        {
            StreamWriter sw = null;
            try
            {
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("settleds", Method.GET);

                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                List<Sealer> dataList = JsonConvert.DeserializeObject<List<Sealer>>(validate);


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creation on s.Account1 equals v.CDSC_Number
                            select s;
                //select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creation on s.Account2 equals v.CDSC_Number
                             select s;
                int six = 0;

                var allresults = dbsel.Concat(dbsel2).ToList().Distinct().OrderByDescending(a => a.Deal);

                
             
                foreach (var ppp in allresults)
                {
                    try
                    {
                        six = db.DealerDGs.ToList().Where(a => a.Deal == ppp.Deal).Count();

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }

                    if (six == 0)
                    {
                        DealerDG my = new DealerDG();
                        my.Account1 = ppp.Account1;
                        my.Account2 = ppp.Account2;
                        my.AccountName1 = ppp.AccountName1;
                        my.AccountName2 = ppp.AccountName2;
                        my.Ack = ppp.Ack;
                        my.DatePosted = ppp.DatePosted;
                        my.SettlementDate = ppp.SettlementDate;
                        my.Deal = ppp.Deal;
                        my.Price = ppp.Price;
                        my.Quantity = ppp.Quantity;
                        my.Security = ppp.Security;
                        my.SettlementDate = ppp.SettlementDate;
                        my.SecurityName = ppp.SecurityName;
                        db.DealerDGs.Add(my);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception f)
            {

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + f.Message);
                sw.Flush();
                sw.Close();
            }

        }
        public static Boolean isTaxable(string s)
        {
            Boolean test = false;
            string nm = "";
            var pace = db.Account_Creation.ToList().Where(a => a.CDSC_Number == s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm == "Taxable")
            {
                test = true;
            }
            return test;
        }
        public static Decimal Buy(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')+sum(BuyCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public static Decimal Sell(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')-sum(SellCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }

            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public static Decimal BR(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT top 1 (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "') as 'Mo'  FROM TransactionCharges where transcode='" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }


    }
}
    


