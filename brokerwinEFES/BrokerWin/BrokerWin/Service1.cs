﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BrokerWin
{
    public partial class Service1 : ServiceBase
    {

        private Timer timer1 = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 180000;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
            Library.WriteErrorLog("Broker Back Office service has started");
        }

        private void timer1_Tick(object sender,ElapsedEventArgs e)
        {
            //Library.WriteErrorLog(DateTime.Now+"Timer Tick has done its job successfully");
            Library.PostCharges();
        }
        protected override void OnStop()
        {
            timer1.Enabled = false; 
            Library.WriteErrorLog("Timer has stopped"); 
        }

    }
}
