//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerWin.DDO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Task
    {
        public int TaskID { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public string TaskUser { get; set; }
        public int TaskUserID { get; set; }
        public decimal percentagedone { get; set; }
        public string status { get; set; }
        public string fromUser { get; set; }
        public string fromUserID { get; set; }
        public System.DateTime TaskDue { get; set; }
    }
}
