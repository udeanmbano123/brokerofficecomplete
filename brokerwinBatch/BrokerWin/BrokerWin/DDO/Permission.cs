//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerWin.DDO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Permission
    {
        public int PermissionID { get; set; }
        public string ControllerName { get; set; }
        public string ViewName { get; set; }
        public string Name { get; set; }
        public string assignedRole { get; set; }
        public string Module { get; set; }
        public bool IsDashboard { get; set; }
        public bool IsWebForm { get; set; }
        public string webFormUrl { get; set; }
        public bool IsActice { get; set; }
    }
}
